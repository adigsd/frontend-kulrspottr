# KulrSpottr

## Challenge
Your challenge will be do develop a single page JavaScript application that display the user a matrix of colored tiles. Task for the user is spot the tile which has a different color. If the  user clicks the right tile the game continues and the user gets the next level of difficulty presented. If the click is wrong the game ends.

**Step 1**

![step 1](imgs/step1.jpg)

**Step 2**

![step 2](imgs/step2.jpg)

**Step 3**

![step 3](imgs/step3.jpg)

With every level the colors should be randomized and the amount of tiles should increase. As you can see the tile which is of different color does not differ it its hue but in the saturation and/or bias/luminance. Please see the [HSB/HSL model](https://en.wikipedia.org/wiki/HSL_and_HSV) for reference.

With every successful step there should also be a counter displayed – starting at zero and incrementing by one for every sucessful step. After the game ends the user should get the opportunity to enter herself with a name into a hall of fame ordered by the reached steps sorted descending and only showing the top 10. (attention: no backend integration needed, just keep it in the browsers mem).

## Rules

1. You should develop this app as a Single Page Application
2. Please build the frontend only – no backend integration necessary
3. Create a **README.md** explaining how to build/run/use the app.
3. You're free to use any framework you like. But name it in your **README.md** and briefly justify why you used it and for which purpose.
4. Think about cross-browser compatibility – it doesn't have to run on IE 6 but it should work and look similar on all common browsers and systems – including mobiles!!!
5. Think about how you are ensuring quality assurance in your process.
6. If you want extra points, code also the script to win the game
7. If you're done check in your solution into any public git repo hoster (github, bitbucket, etc.) and send us the link to GlobalSoftwareDevelopment@adidas-group.com